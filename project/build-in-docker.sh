#!/usr/bin/env bash

# RUN this script under 'root' privileges (with sudo)!

# this script compiles sources and puts the result artifacts into local maven repository that is placed in
# 'maven-repo' volume and can the library be used from 'maven-repo' volume.

docker volume create --name maven-repo

rm -rf target

docker run --rm --name build-project \
    -v "$PWD":/usr/src/mymaven -v maven-repo:/root/.m2 -v "$PWD"/target:/usr/src/mymaven/target \
    -w /usr/src/mymaven maven:3.6-jdk-8-alpine mvn install

chown "$USER:$USER" -R target

